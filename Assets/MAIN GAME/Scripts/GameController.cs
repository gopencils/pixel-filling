﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using UnityEngine.EventSystems;
using GPUInstancer;
using System.Linq;

public class GameController : MonoBehaviour
{
    [Header("Variable")]
    public static GameController instance;
    public int maxLevel;
    public bool isStartGame = false;
    int maxPlusEffect = 0;
    bool isVibrate = false;
    Rigidbody rigid;
    float h, v;
    public float speed;
    Vector3 dir;
    bool isHold = false;
    public static int totalPixel;

    [Header("UI")]
    public GameObject winPanel;
    public GameObject losePanel;
    public Text currentLevelText;
    public Text nextLevelText;
    int currentLevel;
    public Slider levelProgress;
    public Text scoreText;
    public static int score;
    public Canvas canvas;
    public GameObject startGameMenu;
    public InputField levelInput;
    public GameObject nextButton;
    public Text title;
    public Text winMenu_title;
    public Text winMenu_score;

    [Header("Objects")]
    public GameObject plusVarPrefab;
    public GameObject conffeti;
    GameObject conffetiSpawn;
    public GameObject mapReader;
    public GameObject mapFlowEffect;
    public GameObject winBG;

    private void OnEnable()
    {
        //PlayerPrefs.DeleteAll();
        Application.targetFrameRate = 60;
        instance = this;
        rigid = GetComponent<Rigidbody>();
        StartCoroutine(delayRefreshInstancer());
        StartCoroutine(delayStart());
    }

    IEnumerator delayStart()
    {
        yield return new WaitForSeconds(0.01f);
        //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.StartEvent);

        currentLevel = PlayerPrefs.GetInt("currentLevel");
        currentLevelText.text = currentLevel.ToString();
        nextLevelText.text = (currentLevel + 1).ToString();
        score = 0;
        scoreText.text = score.ToString();
        startGameMenu.SetActive(true);
        title.DOColor(new Color32(255,255,255,0), 3);
        levelProgress.maxValue = totalPixel;
        levelProgress.value = 0;
    }

    private void FixedUpdate()
    {
        if (isStartGame)
        {
            Control();          
        }
    }

    private void Control()
    {
        if(Input.GetMouseButtonDown(0))
        {
            isHold = true;
        }

        if (Input.GetMouseButton(0) && isHold)
        {
#if UNITY_EDITOR
            h = Input.GetAxis("Mouse X");
            v = Input.GetAxis("Mouse Y");
#endif
#if UNITY_IOS
            if (Input.touchCount > 0)
            {
                h = Input.touches[0].deltaPosition.x / 8;
                v = Input.touches[0].deltaPosition.y / 8;
            }
#endif
            dir = new Vector3(h, 0, v);
            if (dir != Vector3.zero)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(dir), 10*Time.deltaTime);
            }
            transform.Translate(Vector3.forward * Time.deltaTime * (speed + Mathf.Abs(h) * 5 + Mathf.Abs(v) * 5));
        }
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, -11f, 11f), transform.position.y, Mathf.Clamp(transform.position.z, -4f, 49));

        if (Input.GetMouseButtonUp(0))
        {
            isHold = false;
        }
    }

    public void PlusEffectMethod()
    {
        if (maxPlusEffect < 10)
        {
            Vector3 posSpawn = scoreText.transform.position;
            StartCoroutine(PlusEffect(posSpawn));
        }
    }

    IEnumerator PlusEffect(Vector3 pos)
    {
        maxPlusEffect++;
        if (!UnityEngine.iOS.Device.generation.ToString().Contains("5") && !isVibrate)
        {
            isVibrate = true;
            StartCoroutine(delayVibrate());
            MMVibrationManager.Haptic(HapticTypes.LightImpact);
        }
        var plusVar = Instantiate(plusVarPrefab);
        plusVar.transform.SetParent(canvas.transform);
        plusVar.transform.localScale = new Vector3(1, 1, 1);
        //plusVar.transform.position = worldToUISpace(canvas, pos);
        plusVar.transform.position = new Vector3(pos.x + Random.Range(-50,50), pos.y + Random.Range(-100, -75), pos.z);
        plusVar.GetComponent<Text>().DOColor(new Color32(255, 255, 255, 0), 1f);
        plusVar.SetActive(true);
        plusVar.transform.DOMoveY(plusVar.transform.position.y + Random.Range(50, 90), 0.5f);
        plusVar.transform.DOMoveX(scoreText.transform.position.x, 0.5f);
        Destroy(plusVar, 0.5f);
        yield return new WaitForSeconds(0.01f);
        maxPlusEffect--;
    }

    IEnumerator delayVibrate()
    {
        yield return new WaitForSeconds(0.2f);
        isVibrate = false;
    }

    public Vector3 worldToUISpace(Canvas parentCanvas, Vector3 worldPos)
    {
        Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPos);
        Vector2 movePos;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(parentCanvas.transform as RectTransform, screenPos, parentCanvas.worldCamera, out movePos);
        return parentCanvas.transform.TransformPoint(movePos);
    }

    public void ButtonStartGame()
    {
        startGameMenu.SetActive(false);
        isStartGame = true;
        isHold = true;
    }

    IEnumerator Win()
    {
        var bomb = GameObject.FindGameObjectsWithTag("Hole");
        var hole = GameObject.FindGameObjectsWithTag("Wall");
        var wall = GameObject.FindGameObjectsWithTag("Wall");
        if(bomb.Length > 0)
        {
            foreach(var item in bomb)
            {
                Destroy(item);
            }
        }
        if (hole.Length > 0)
        {
            foreach (var item in hole)
            {
                Destroy(item);
            }
        }
        if (wall.Length > 0)
        {
            foreach (var item in wall)
            {
                Destroy(item);
            }
        }
        yield return new WaitForSeconds(0.01f);
        if (isStartGame)
        {
            //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);
            isStartGame = false;
            losePanel.SetActive(false);
            conffetiSpawn = Instantiate(conffeti);
            winMenu_title.text = "LEVEL " + currentLevel.ToString();
            currentLevel++;
            if (currentLevel > maxLevel)
            {
                currentLevel = 0;
            }
            PlayerPrefs.SetInt("currentLevel", currentLevel);
            winMenu_score.text = score.ToString();
            yield return new WaitForSeconds(0.1f);
            winBG.SetActive(true);
            winBG.GetComponent<MeshRenderer>().material.DOFade(1, 1);
            winPanel.SetActive(true);
            scoreText.gameObject.SetActive(false);
            levelProgress.gameObject.SetActive(false);
            //MapFlowEffect.instance.RunMapFlowEffect();
        }
    }

    IEnumerator delayRefreshInstancer()
    {
        yield return new WaitForSeconds(0.01f);
        AddRemoveInstances.instance.Setup();
    }

    public void Lose()
    {
        if (isStartGame)
        {
            //AnalyticsManager.instance.CallEvent(AnalyticsManager.EventType.EndEvent);
            isStartGame = false;
            StartCoroutine(delayLose());
        }
    }

    IEnumerator delayLose()
    {
        yield return new WaitForSeconds(1);
        losePanel.SetActive(true);
    }

    public void LoadScene()
    {
        //MapFlowEffect.instance.isStop = true;
        winPanel.SetActive(false);
        losePanel.SetActive(false);
        var temp = conffetiSpawn;
        Destroy(temp);
        SceneManager.LoadScene(0);
    }

    public void OnChangeMap()
    {
        if (levelInput != null)
        {
            int level = int.Parse(levelInput.text.ToString());
            Debug.Log(level);
            if (level < maxLevel)
            {
                PlayerPrefs.SetInt("currentLevel", level);
                SceneManager.LoadScene(0);
            }
        }
    }

    public void ButtonNextLevel()
    {
        title.DOKill();
        isStartGame = true;
        currentLevel++;
        if (currentLevel > maxLevel)
        {
            currentLevel = 0;
        }
        PlayerPrefs.SetInt("currentLevel", currentLevel);
        SceneManager.LoadScene(0);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pixel") && !other.GetComponent<Tile>().isCheck && isStartGame)
        {
            
        }
    }
}
