using UnityEngine;
using GPUInstancer;
using DG.Tweening;

public class Tile : MonoBehaviour
{
    public Color tileColor;
    private Renderer meshRenderer;
    public bool isCheck = false;
    GameController gameController;

    private void OnEnable()
    {
        Init();
        gameController = GameObject.FindGameObjectWithTag("Player").GetComponent<GameController>();
    }

    public void Init()
    {
        if (meshRenderer == null)
            meshRenderer = GetComponent<Renderer>();
    }

    public void SetTransfrom(Vector3 pos,Vector3 scale)
    {
        transform.localPosition = pos;
        transform.localScale = new Vector3(scale.x,scale.y,scale.z);
    }

    public void SetColor(Color inputColor)
    {
        tileColor = inputColor;       
        meshRenderer.material.color = tileColor;
        tag = "Pixel";
    }

    public void Check()
    {
        isCheck = true;
    }   
}
