using UnityEngine;
using System.Collections.Generic;

public class LevelGenerator : MonoBehaviour {

    public static LevelGenerator instance;
    public List<Texture2D> list2DMaps = new List<Texture2D>();
    public Texture2D map;
    public Tile tilePrefab;
    public GameObject parentObject;
    Transform currentParent;
    Vector3 originalPos;
    float width;
    public GameObject l, r, u, d;
    int min = 1000, max = -1000;

    void OnEnable()
    {
        instance = this;
        var currentLevel = PlayerPrefs.GetInt("currentLevel");
        map = list2DMaps[currentLevel];
        originalPos = parentObject.transform.position;
        currentParent = parentObject.transform;
        GameController.totalPixel = 0;
        GenerateMap(map);
        parentObject.transform.position = originalPos;
        parentObject.transform.localScale = Vector3.one * (15 / width);
    }

    private void GenerateMap(Texture2D texture)
    {
        width = texture.width;
        float ratioX = texture.width;
        float ratioY = texture.height;
        float ratio;
        if (ratioY > ratioX)
        {
            ratio = ratioX / ratioY;
        }
        else
        {
            ratio = ratioY / ratioX;
        }
        if (ratio < 0.6f && ratio > 0.4f)
        {
            ratio = 1;
        }
        Debug.Log(ratio);

        //Vector3 positionTileParent = new Vector3(-((texture.width - 1) * ratio / 2), 0, -((texture.height - 1) * ratio / 2));
        //currentParent.localPosition = positionTileParent;
        bool isColor = false;
        for (int x = 0; x < texture.width - 1; x++)
        {
            min = 1000;
            max = -1000;
            for (int y = 0; y < texture.height - 1; y++)
            {
                GenerateTile(texture, x, y, ratio);
                Color pixelColor = texture.GetPixel(x, y);
                if (pixelColor.a == 0 || pixelColor == null)
                {
                    if (isColor)
                    {
                        GenerateBound(texture, x, y--, ratio);
                    }
                    isColor = false;
                }
                else
                {
                    if (!isColor)
                    {
                        GenerateBound(texture, x, y, ratio);
                    }
                    isColor = true;
                    if (y < min)
                        min = y;
                    if (y > max)
                        max = y;
                }
            }
            min--;
            max++;
            GenerateBound(texture, x, min, ratio);
            GenerateBound(texture, x, max, ratio);
        }

        for (int y = 0; y < texture.height - 1; y++)
        {
            min = 1000;
            max = -1000;
            for (int x = 0; x < texture.width - 1; x++)
            {
                Color pixelColor = texture.GetPixel(x, y);
                if (pixelColor.a == 0 || pixelColor == null)
                {
                    if (isColor)
                    {
                        GenerateBound(texture, x--, y, ratio);
                    }
                    isColor = false;
                }
                else
                {
                    if (!isColor)
                    {
                        GenerateBound(texture, x, y, ratio);
                    }
                    isColor = true;
                    if (x < min)
                        min = x;
                    if (x > max)
                        max = x;
                }
            }
            min--;
            max++;
            GenerateBound(texture, min, y, ratio);
            GenerateBound(texture, max, y, ratio);
        }
    }

    private void GenerateBound(Texture2D texture, int x, int y, float ratio)
    {
        Tile instance;
        instance = Instantiate(tilePrefab);
        instance.transform.SetParent(currentParent);
        Color pixelColor = Color.white;
        Vector3 pos = new Vector3(x - texture.width / 2, 0, y) * ratio;
        Vector3 scale = new Vector3(ratio, 3, ratio);

        instance.Init();
        instance.SetTransfrom(pos, scale);
        instance.SetColor(pixelColor);
    }

    private void GenerateTile(Texture2D texture, int x, int y, float ratio)
    {
        Tile instance;
        instance = Instantiate(tilePrefab);
        instance.transform.SetParent(currentParent);
        Color pixelColor = texture.GetPixel(x, y);
        Vector3 pos = new Vector3(x - texture.width / 2, 0, y) * ratio;
        Vector3 scale = Vector3.one * ratio;

        if (pixelColor.a == 0 || pixelColor == null)
        {
            return;
            //pixelColor = Color.white;
        }
        else
        {
            GameController.totalPixel++;
        }

        instance.Init();
        instance.SetTransfrom(pos, scale);
        instance.SetColor(pixelColor);
    }

}
